"""
Adds a new device that that saves all commands to a file instead of doing any actual work. This can
be used to for testing or for using other software that monitors a file for commands.

License
=======

For full license information, see the LICENSE.md file.

The Yombo team and other contributors hopes that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.

.. moduleauthor:: Mitch Schwenk <mitch-gw@yombo.net>
:copyright: 2012-2019 Yombo
:license: YRPL 1.6
"""

import json
import time

from yombo.core.module import YomboModule
from yombo.core.log import get_logger
from yombo.utils.filewriter import FileWriter

logger = get_logger("modules.devicetofile")


class DeviceToFile(YomboModule):
    """
    Acts like a device - any commands it receives, it will write to the file. Provides full
    device command responses. This module is typically used for testing and development.

    .. moduleauthor:: Mitch Schwenk <mitch-gw@yombo.net>

    :copyright: Copyright 2017-2019 by Yombo.
    :license: LICENSE for details.
    """
    
    def _init_(self, **kwargs):
        # Get a file name save data to..
        print("device to file...INIT!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
        if "file" in self._module_variables_cached:
          filename = self._module_variables_cached["file"]['values'][0]
          if filename is None:
              filename = "devicetofile.txt"
        else:
          logger.warn("No 'logfile' set for log writing, using default: 'logwriter.txt'")
          filename = "devicetofile.txt"

        self.file_out = FileWriter(filename=filename)

    def _unload_(self, **kwargs):
        """
        Flush and close the output log file.
        """
        self.file_out.close()

    def _device_command_(self, **kwargs):
        """
        Received a device command.

        :param kwags: Contains 'device' and 'command'.
        :return: None
        """
        device = kwargs['device']
        request_id = kwargs['request_id']
        if self._is_my_device(device) is False:
            logger.debug("Skipping _device_command_ call since '{device_type_id}' not in here: {device_types}",
                    device_type_id=device.device_type_id, device_types=self._module_device_types_cached)
            return  # not meant for us.

        device.device_command_processing(request_id,
                                         message=_('module::devicetofile::processing', 'Handled by DeviceToFile module.'))
        device.device_command_pending(request_id,
                                      message=_('module::devicetofile::processing::pending', 'About to write to file'))

        command = kwargs['command']

        self.file_out.write("%s\n" % json.dumps(
            {
                'time': int(time.time()),
                'device_id': device.device_id,
                'device_label': device.label,
                'command_id': command.command_id,
                'command_label': command.label,
                'inputs': kwargs['inputs'],
                'request_id': request_id,
            }
            )
        )
        device.set_status(
            human_status=command.label,
            human_message="%s is now %s" % (device.label, command.label),
            command=command.label,
            request_id=request_id)

        device.device_command_done(request_id, message=_('module.devicetofile_done', 'Device to file done.'))
