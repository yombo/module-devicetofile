Summary
=======

This simple module writes all device commands to a text file. It only
reacts to devices assigned to the "device_to_file" device_type. This
module is used primarily for testing and development, or if you want
a simple log-like system to be included in a scene.

Learn about [Yombo Gateway](https://yombo.net/) or
[Get started today](https://yg2.in/start)

Installation
============

Simply mark this module as being used by the gateway, and the gateway will
download and install this module automatically.

License
=======

The [Yombo](https://yombo.net/) team and other contributors hopes that it
will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See Yombo Gateway LICENSE file for full details.

